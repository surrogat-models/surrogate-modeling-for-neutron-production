# Surrogate Modeling For Neutron Production

[![License: Apache 2.0](https://img.shields.io/badge/License-cc-Attribution4.svg)](https://git.rwth-aachen.de/surrogat-models/surrogate-modeling-for-neutron-production/-/blob/main/LICENSE)
<!-- [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.6383868.svg)](https://doi.org/10.5281/zenodo.6383868) -->



## Table of contents
1. [Introduction](#introduction)
2. [Strucuture of Folder Tree](#folderstructure)
3. [Known Problems](#bugs)
4. [Publications & Links](#publications)
5. [Contributers](#contributors)
6. [Funding](#funding)

## 1. Introduction <a name="introduction"></a>

This repository cotains the network training scripts and resulting models for the neutron production paper [1].
The scripts utilize Keras [2] and tensorflow 2 [3].
It is part of the LOEWE NP research cluster at TU Darmstadt [4] and the work was done at the Fachgebiet Beschleunigerphysik at TU Darmstadt [5]

## 2. Structure of Folder Tree <a name="folderstructure"></a>

The data is ordered in different folders, which all belond to the simulations ecosystem. 
* `./NeutronSurrogate_Notebook_Train.ipynb`: Jupyter Notebook for training the model.
* `./NeutronSurrogate_Notebook_Application.ipynb`: Jupyter Notebook with example applications of the models.
* `./*.csv`: Several csv files containing datasets. They are labeled according to the name used in the corresponding paper. Please refer to the paper for references.
* `./neutron_model/*.h5`: Several models trained from the bootstrap approach and for the data, as explained in the paper. They are needed to reproduce the figures in the application notebook.

## 3. Known Problems <a name="bugs"></a>
The data for training has to be fetched from the corresponding repository to allow for training. ([gitlab](https://git.rwth-aachen.de/surrogat-models/neutron-data)|[tudata]).


## 4. Publications & Links <a name="publications"></a>

[1] Neutron Yield Predictions with Artificial Neural Networks: A Predictive Modeling Approach

[2] [Keras](https://keras.io/)

[3] [tensorflow](https://www.tensorflow.org/)

[4] [LOEWE center for Nuclear Photonics](https://www.ikp.tu-darmstadt.de/nuclearphotonics/nuclear_photonics/index.en.jsp) 

[5] [Accelerator Pyhsics at TEMF, TU Darmstadt](https://www.bp.tu-darmstadt.de/fachgebiet_beschleunigerphysik/index.en.jsp)

## 5. Contributors <a name="contributors"></a>
Contributors include (alphabetically): 
*   B. Schmitz

## 6. Funding <a name="funding"></a>
* This work was funded by HMWK through the LOEWE center “Nuclear Photonics”.
